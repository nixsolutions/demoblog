﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrototypeApp.BLL.Services.Abstract;
using PrototypeApp.BLL.Services.Concrete;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Packaging;

namespace PrototypeApp.Infrastructure.IoC
{
    public class ServicesPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            var webRequestLifestyle = new WebRequestLifestyle();

            container.Register<IUserService, UserService>(webRequestLifestyle);
            container.Register<IUserManagementService, UserManagementService>(webRequestLifestyle);
            container.Register<IBlogPostService, BlogPostService>(webRequestLifestyle);
        }
    }
}