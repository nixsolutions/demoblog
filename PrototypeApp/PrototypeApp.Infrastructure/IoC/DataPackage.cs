﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Repository;
using PrototypeApp.DAL.Context;
using PrototypeApp.DAL.Repository;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Packaging;

namespace PrototypeApp.Infrastructure.IoC
{
    public class DataPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            var webRequestLifestyle = new WebRequestLifestyle();

            container.Register(typeof(IRepository<>), typeof(EFRepository<>), webRequestLifestyle);
            container.Register<IUnitOfWork, EFUnitOfWork>(webRequestLifestyle);
            container.Register<IdentityDbContext<User>, ApplicationContext>(webRequestLifestyle);
            container.Register<UserManager<User>>(webRequestLifestyle);
            container.Register<IUserStore<User>>(() => new UserStore<User>(container.GetInstance<IdentityDbContext<User>>()), webRequestLifestyle);
        }
    }
}