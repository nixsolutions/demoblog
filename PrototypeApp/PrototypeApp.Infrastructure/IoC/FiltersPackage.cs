﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.Abstract;
using PrototypeApp.BLL.Filters.Concrete;
using PrototypeApp.BLL.Filters.FilterParameters;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Packaging;

namespace PrototypeApp.Infrastructure.IoC
{
    public class FiltersPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            var webRequestLifestyle = new WebRequestLifestyle();

            container.Register<IFilter<User, UsersFilterParameters>, UserFilter>(webRequestLifestyle);
            container.Register<IFilter<BlogPost, BlogPostFilterParameters>, BlogPostFilter>(webRequestLifestyle);
        }
    }
}