﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace PrototypeApp.Infrastructure.MapperConfig
{
    public static class MapperConfig
    {
        public static void Configure()
        {
            string webProjectName = "PrototypeApp.Web";
            string bllProjectName = "PrototypeApp.BLL";

            Mapper.Initialize(cfg => cfg.AddProfiles(webProjectName, bllProjectName));
        }
    }
}