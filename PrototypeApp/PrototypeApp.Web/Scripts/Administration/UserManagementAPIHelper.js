﻿function editUser(user, successCallback, errorCallback) {
    $.ajax({
        url: '/UserManagement/EditUser/',
        type: 'PUT',
        data: user,
        success: successCallback,
        error: errorCallback
    });
}

function deleteUser(userId, successCallback, errorCallback) {
    $.ajax({
        url: '/UserManagement/DeleteUser?userId=' + userId,
        type: 'DELETE',
        success: successCallback,
        error: errorCallback
    });
}

function addUser(user, successCallback, errorCallback) {
    $.ajax({
        url: '/UserManagement/AddUser',
        type: 'POST',
        data: user,
        success: successCallback,
        error: errorCallback
    });
}

function getErrorsFromResponseByKey(response, key) {
    var responseKeyErrors = response[key];
    var errorsCollection = [];
    for (var property in responseKeyErrors) {
        errorsCollection.push(responseKeyErrors[property].ErrorMessage);
    }

    return errorsCollection;
}