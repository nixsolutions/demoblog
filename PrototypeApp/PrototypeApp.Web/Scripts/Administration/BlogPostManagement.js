﻿var confirmationModalOptions = {
    message: "Are you sure you want to delete this blog post?",
    title: 'Confirmation',
    label: "Yes"
};

var selectorContainer = {
    editBlogPostIcon: ".edit-blog-post-icon",
    deleteBlogPostIcon: ".delete-blog-post-icon",
    postsTable: "#postsTable"
}

var customAttributes = {
    blogPostId: "blogpostid",
    totalPosts: "total-posts",
    pageSize: "page-size",
    currentPageNumber: "current-page"
}

$(selectorContainer.editBlogPostIcon).click(function () {
    var editPostRoute = "Edit?postId=";
    var blogPostId = $(this).data(customAttributes.blogPostId);
    location.href = editPostRoute + blogPostId;
});

$(selectorContainer.deleteBlogPostIcon).click(function () {
    var blogPostId = $(this).data(customAttributes.blogPostId);
    eModal.confirm(confirmationModalOptions)
        .then(function () {
            deleteBlogPost(blogPostId, function () {
                if (getNumberOfPostsOnCurrentPage() === 1) {
                    openPreviousPostsPage();
                } else {
                    reloadPage();
                }
            });
        });
});

function reloadPage() {
    location.reload();
}

function openPreviousPostsPage() {
    var pageSize = $(selectorContainer.postsTable).data(customAttributes.pageSize);
    var currentPageNumber = $(selectorContainer.postsTable).data(customAttributes.currentPageNumber);
    window.location.href = "?PaginationModel.PageNumber=" + (currentPageNumber - 1) + "&PaginationModel.RecordsPerPage=" + pageSize;
}

function getNumberOfPostsOnCurrentPage() {
    var totalPosts = $(selectorContainer.postsTable).data(customAttributes.totalPosts);
    var pageSize = $(selectorContainer.postsTable).data(customAttributes.pageSize);
    var currentPageNumber = $(selectorContainer.postsTable).data(customAttributes.currentPageNumber);

    return totalPosts - (currentPageNumber - 1) * pageSize;
}