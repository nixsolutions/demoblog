﻿function deleteBlogPost(blogPostId, successCallback, errorCallback) {
    $.ajax({
        url: '/BlogPostManagement/Delete?postId=' + blogPostId,
        type: 'DELETE',
        success: successCallback,
        error: errorCallback
    });
}