﻿var confirmationModalOptions = {
    message: "Are you sure you want to delete this user?",
    title: 'Confirmation',
    label: "Yes"
};

var selectorContainer = {
    deleteUserIcon: ".delete-user-icon",
    editUserIcon: ".edit-user-icon",
    addUserButton: "#addUserButton",
    usersTable: "#usersTable"
}

var addUserModalSelectors = {
    modal: "#addUserModal",
    emailGroup:"#addModalEmail",
    emailInput: "#addModalEmailInput",
    emailValidation:"#addModalEmailValidation",
    passwordInput: "#addModalPasswordInput",
    passwordGroup: "#addModalPassword",
    passwordValidation: "#addModalPasswordValidation",
    roleSelector: "#addModalRoleSelector",
    saveButton: "#addModalSaveButton"
}

var editUserSelectors = {
    modal: "#editUserModal",
    emailValidation: "#editModalEmailValidation",
    emailGroup: "#editModalEmailGroup",
    emailInput: "#editModalEmailInput",
    roleSelector: "#editModalRoleSelector",
    saveChangesButton: "#editModalSaveChanges"
}

var customAttributes = {
    userId: "userid",
    userEmail: "user-email",
    userRole: "user-role",
    totalUsers: "total-users",
    pageSize: "page-size",
    currentPageNumber: "current-page"
}

$(selectorContainer.deleteUserIcon).click(function () {
    var userId = $(this).data(customAttributes.userId);
    eModal.confirm(confirmationModalOptions)
        .then(function () {
            deleteUser(userId, function () {
                if (getNumberOfUsersOnCurrentPage() === 1) {
                    openPreviousUsersPage(currentPageNumber, pageSize);
                } else {
                    reloadPage();
                }
            });
        });
});

$(selectorContainer.editUserIcon).click(function () {
    var userEmail = $(this).data(customAttributes.userEmail);
    var userId = $(this).data(customAttributes.userId);
    var userRole = $(this).data(customAttributes.userRole);

    fillEditUserModal(userEmail, userRole, userId);
    $(editUserSelectors.modal).modal({ show: true });
});

$(selectorContainer.addUserButton).click(function () {
    $(addUserModalSelectors.modal).modal({ show: true });
});

$(editUserSelectors.modal).on('hidden.bs.modal', function () {
    $(editUserSelectors.emailValidation).text("");
    removeErrorClass($(editUserSelectors.emailGroup));
});

$(addUserModalSelectors.modal).on('hidden.bs.modal', function () {
    $(addUserModalSelectors.emailValidation).text("");
    removeErrorClass($(addUserModalSelectors.emailGroup));
    $(addUserModalSelectors.passwordValidation).text("");
    removeErrorClass($(addUserModalSelectors.passwordGroup));
});

function fillEditUserModal(userEmail, userRole, userId) {
    $(editUserSelectors.emailInput).val(userEmail);
    $(editUserSelectors.roleSelector).val(userRole);
    $(editUserSelectors.modal).data(customAttributes.userId, userId);
}

$(editUserSelectors.saveChangesButton).click(function () {
    var emailKey = "Email";
    var newUser = {
        Id: $(editUserSelectors.modal).data(customAttributes.userId),
        Email: $(editUserSelectors.emailInput).val(),
        Role: $(editUserSelectors.roleSelector).val()
    };

    editUser(newUser, function () {
        $(editUserSelectors.modal).modal({ hide: true });
        location.reload();
    }, function (data) {
        addErrorClass($(editUserSelectors.emailGroup));
        $(editUserSelectors.emailValidation).text(getErrorsFromResponseByKey(data.responseJSON, emailKey));
    });
});

$(addUserModalSelectors.saveButton).click(function () {
    var emailKey = "Email";
    var passwordKey = "Password";
    var newUser = {
        Email: $(addUserModalSelectors.emailInput).val(),
        Password: $(addUserModalSelectors.passwordInput).val(),
        Role: $(addUserModalSelectors.roleSelector).val()
    };

    addUser(newUser, function () {
        $(addUserModalSelectors.modal).modal({ hide: true });
        location.reload();
    }, function (data) {
        var emailErrors = getErrorsFromResponseByKey(data.responseJSON, emailKey);
        var passwordErrors = getErrorsFromResponseByKey(data.responseJSON, passwordKey);
        showValidationErrors($(addUserModalSelectors.emailGroup), $(addUserModalSelectors.emailValidation), emailErrors);
        showValidationErrors($(addUserModalSelectors.passwordGroup), $(addUserModalSelectors.passwordValidation), passwordErrors);
    });
});

function getNumberOfUsersOnCurrentPage() {
    var totalUsers = $(selectorContainer.usersTable).data(customAttributes.totalUsers);
    var pageSize = $(selectorContainer.usersTable).data(customAttributes.pageSize);
    var currentPageNumber = $(selectorContainer.usersTable).data(customAttributes.currentPageNumber);
    return totalUsers - (currentPageNumber - 1) * pageSize;
}

function openPreviousUsersPage(currentPageNumber, pageSize) {
    window.location.href = "GetUsers?PageNumber=" + (currentPageNumber - 1) + "&RecordsPerPage=" + pageSize;
}

function reloadPage() {
    location.reload();
}

function addErrorClass(element) {
    element.addClass("has-error");
}

function removeErrorClass(element) {
    element.removeClass("has-error");
}

function showValidationErrors(element, validationLabel, errors) {
    if (errors.length > 0) {
        addErrorClass(element);
        validationLabel.text(errors.join());
    } else {
        removeErrorClass(element);
        validationLabel.text("");
    }
}