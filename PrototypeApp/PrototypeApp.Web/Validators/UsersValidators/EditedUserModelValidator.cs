﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Models.Users;
using PrototypeApp.Web.Resources;

namespace PrototypeApp.Web.Validators.UsersValidators
{
    public class EditedUserModelValidator : AbstractValidator<EditedUserModel>
    {
        public EditedUserModelValidator()
        {
            this.RuleFor(m => m.Email).NotNull().WithMessage(ValidationMessages.EmptyEmail);
            this.RuleFor(m => m.Email).EmailAddress().WithMessage(ValidationMessages.WrongEmailFormat);
        }
    }
}