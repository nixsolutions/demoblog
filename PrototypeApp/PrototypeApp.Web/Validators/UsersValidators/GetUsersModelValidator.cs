﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Models.Users;

namespace PrototypeApp.Web.Validators.UsersValidators
{
    public class GetUsersModelValidator : AbstractValidator<GetUsersModel>
    {
        public GetUsersModelValidator()
        {
            this.RuleFor(model => model.PaginationModel).SetValidator(new PaginationModelValidator());
        }
    }
}