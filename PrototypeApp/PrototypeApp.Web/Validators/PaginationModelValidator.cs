﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Models;

namespace PrototypeApp.Web.Validators
{
    public class PaginationModelValidator : AbstractValidator<PaginationModel>
    {
        public PaginationModelValidator()
        {
            this.RuleFor(model => model.PageNumber).GreaterThan(0);
        }
    }
}