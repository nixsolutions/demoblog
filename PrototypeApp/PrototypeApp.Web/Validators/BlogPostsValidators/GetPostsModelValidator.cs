﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Models.BlogPosts;

namespace PrototypeApp.Web.Validators.BlogPostsValidators
{
    public class GetPostsModelValidator : AbstractValidator<GetPostsModel>
    {
        public GetPostsModelValidator()
        {
            this.RuleFor(model => model.PaginationModel).SetValidator(new PaginationModelValidator());
        }
    }
}