﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Models.BlogPosts;
using PrototypeApp.Web.Resources;

namespace PrototypeApp.Web.Validators.BlogPostsValidators
{
    public class BlogPostModelValidator : AbstractValidator<BlogPostModel>
    {
        public BlogPostModelValidator()
        {
            this.RuleFor(m => m.Title).NotEmpty().WithMessage(ValidationMessages.PostTitleIsEmpty);
            this.RuleFor(m => m.Content).NotEmpty().WithMessage(ValidationMessages.PostContentIsEmpty);
            this.RuleFor(m => m.Alias).NotEmpty().WithMessage(ValidationMessages.PostAliasIsEmpty);
        }
    }
}