﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using PrototypeApp.Web.Constants;
using PrototypeApp.Web.Models;
using PrototypeApp.Web.Resources;

namespace PrototypeApp.Web.Validators
{
    public class LoginModelValidator : AbstractValidator<LoginModel>
    {
        public LoginModelValidator()
        {
            this.RuleFor(m => m.Email).NotNull().WithMessage(ValidationMessages.EmptyEmail);
            this.RuleFor(model => model.Email).EmailAddress().WithMessage(ValidationMessages.WrongEmailFormat);
            this.RuleFor(m => m.Password).NotNull().WithMessage(ValidationMessages.PasswordLength);
            this.RuleFor(m => m.Password).Length(ValidationConstants.MinimalPasswordLength, ValidationConstants.MaximalPasswordLength)
                .WithMessage(ValidationMessages.PasswordLength);
        }
    }
}