﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.Concrete;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.Web.ViewModels
{
    public class GetUsersViewModel
    {
        public IPagedList<UserDTO> Users { get; set; }

        public UsersFilterParameters UserFilterParameters { get; set; }
    }
}