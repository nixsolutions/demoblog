﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.Web.ViewModels
{
    public class BlogPostsViewModel
    {
        public IPagedList<BlogPostDTO> BlogPosts { get; set; }

        public BlogPostFilterParameters FilterParameters { get; set; }
    }
}