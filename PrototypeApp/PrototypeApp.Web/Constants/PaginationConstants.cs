﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrototypeApp.Web.Constants
{
    public static class PaginationConstants
    {
        public const int DefaultRecordsPerGridPage = 10;

        public const int DefaultNumberOfPostsOnMainPage = 9;

        public const int DefaultPageNumber = 1;
    }
}