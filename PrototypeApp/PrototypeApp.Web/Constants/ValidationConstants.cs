﻿namespace PrototypeApp.Web.Constants
{
    public static class ValidationConstants
    {
        public const int MinimalPasswordLength = 6;

        public const int MaximalPasswordLength = 50;
    }
}