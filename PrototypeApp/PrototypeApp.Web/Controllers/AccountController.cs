﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Resources;
using PrototypeApp.BLL.Services.Abstract;
using PrototypeApp.Web.Helpers;
using PrototypeApp.Web.Models;
using PrototypeApp.Web.Resources;

namespace PrototypeApp.Web.Controllers
{
    public class AccountController : Controller
    {
        private const string EmailModelStateKey = "Email";
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        public ActionResult Register()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegistrationModel registrationModel)
        {
            if (ModelState.IsValid)
            {
                var user = Mapper.Map<RegistrationModel, UserDTO>(registrationModel);
                var result = await this.userService.RegisterAsync(user);
                if (!result.Succeeded)
                {
                    IdentityHelper.SetModelStateErrors(this.ModelState, result, EmailModelStateKey);
                    return this.View(registrationModel);
                }

                return this.RedirectToAction("Index", "Home");
            }

            return this.View(registrationModel);
        }

        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Home");
            }

            ViewBag.returnUrl = returnUrl;
            return this.View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ClaimsIdentity claim = await this.userService.Authenticate(Mapper.Map<LoginModel, UserDTO>(model));
                if (claim == null)
                {
                    ModelState.AddModelError(EmailModelStateKey, ValidationMessages.WrongEmailOrPassword);
                    return this.View(model);
                }

                var authenticationManager = HttpContext.GetOwinContext().Authentication;
                authenticationManager.SignOut();
                authenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                },
                claim);
                return this.RedirectToAction("Index", "Home");
            }

            ViewBag.returnUrl = returnUrl;
            return this.View(model);
        }

        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return this.RedirectToAction("Login");
        }
    }
}