﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using PagedList;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.BLL.Services.Abstract;
using PrototypeApp.Web.Helpers;
using PrototypeApp.Web.Models.BlogPosts;
using PrototypeApp.Web.Resources;
using PrototypeApp.Web.ViewModels;

namespace PrototypeApp.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class BlogPostManagementController : Controller
    {
        private IBlogPostService blogPostService;

        public BlogPostManagementController(IBlogPostService blogPostService)
        {
            this.blogPostService = blogPostService;
        }

        [AllowAnonymous]
        public ActionResult Get(string alias)
        {
            var requestedPost = this.blogPostService.GetBlogPostByAlias(alias);
            return this.View(Mapper.Map<BlogPostDTO, BlogPostModel>(requestedPost));
        }

        public ActionResult GetPosts(GetPostsModel getPostsModel)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int totalNumberOfPosts;
            var pagination = getPostsModel.PaginationModel;
            if (!PaginationHelper.IsPaginationModelFilled(pagination))
            {
                PaginationHelper.FillPaginationWithDefaultGridParameters(pagination);
            }

            var filterParameters = Mapper.Map<BlogPostFilterParametersModel, BlogPostFilterParameters>(getPostsModel.FilterParameters);
            var requestedPosts = this.blogPostService.Get(filterParameters,
                pagination.RecordsToSkip,
                pagination.RecordsPerPage,
                out totalNumberOfPosts).ToList();

            StaticPagedList<BlogPostDTO> blogPostsList = new StaticPagedList<BlogPostDTO>(requestedPosts,
                pagination.PageNumber,
                pagination.RecordsPerPage,
                totalNumberOfPosts);

            return this.View("BlogPosts", new BlogPostsViewModel() { BlogPosts = blogPostsList, FilterParameters = filterParameters });
        }

        [HttpGet]
        public ActionResult Add()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(BlogPostModel blogPostModel)
        {
            this.FillModelStateBlogPostErrors(blogPostModel);
            if (!ModelState.IsValid)
            {
                return this.View(blogPostModel);
            }

            await this.blogPostService.Add(Mapper.Map<BlogPostModel, BlogPostDTO>(blogPostModel), User.Identity.Name);
            return this.RedirectToAction("GetPosts");
        }

        [HttpGet]
        public ActionResult Edit(int postId)
        {
            var requestedBlogPost = this.blogPostService.GetBlogPostById(postId);
            return this.View(Mapper.Map<BlogPostDTO, BlogPostModel>(requestedBlogPost));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(BlogPostModel blogPostModel)
        {
            this.FillModelStateBlogPostErrors(blogPostModel);
            if (!ModelState.IsValid)
            {
                return this.View(blogPostModel);
            }

            await this.blogPostService.Edit(Mapper.Map<BlogPostModel, BlogPostDTO>(blogPostModel), User.Identity.Name);
            return this.RedirectToAction("GetPosts");
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int postId)
        {
            await this.blogPostService.Delete(postId, User.Identity.Name);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private void FillModelStateBlogPostErrors(BlogPostModel blogPostModel)
        {
            string aliasModelStateKey = "Alias";
            string titleModelStateKey = "Title";

            if (this.blogPostService.IsAliasTaken(blogPostModel.Alias, blogPostModel.Id))
            {
                this.ModelState.AddModelError(aliasModelStateKey, ValidationMessages.AliasIsAlredyTaken);
            }

            if (this.blogPostService.IsTitleTaken(blogPostModel.Title, blogPostModel.Id))
            {
                this.ModelState.AddModelError(titleModelStateKey, ValidationMessages.TitleIsAlreadyTaken);
            }
        }
    }
}