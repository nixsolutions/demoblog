﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PagedList;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.BLL.Services.Abstract;
using PrototypeApp.Web.Helpers;
using PrototypeApp.Web.Models.BlogPosts;
using PrototypeApp.Web.ViewModels;

namespace PrototypeApp.Web.Controllers
{
    public class HomeController : Controller
    {
        private IBlogPostService blogPostService;

        public HomeController(IBlogPostService blogPostService)
        {
            this.blogPostService = blogPostService;
        }

        public ActionResult Index(GetPostsModel getPostsModel)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int totalNumberOfPosts;
            var pagination = getPostsModel.PaginationModel;
            if (!PaginationHelper.IsPaginationModelFilled(pagination))
            {
                PaginationHelper.FillPaginationWithDefaultPostParameters(pagination);
            }

            var filterParameters = Mapper.Map<BlogPostFilterParametersModel, BlogPostFilterParameters>(getPostsModel.FilterParameters);
            var requestedPosts = this.blogPostService.Get(filterParameters,
                pagination.RecordsToSkip,
                pagination.RecordsPerPage,
                out totalNumberOfPosts).ToList();

            StaticPagedList<BlogPostDTO> blogPostsList = new StaticPagedList<BlogPostDTO>(requestedPosts,
                pagination.PageNumber,
                pagination.RecordsPerPage,
                totalNumberOfPosts);

            return this.View(new BlogPostsViewModel() { BlogPosts = blogPostsList, FilterParameters = filterParameters });
        }

        public ActionResult Credentials()
        {
            return this.View();
        }
    }
}