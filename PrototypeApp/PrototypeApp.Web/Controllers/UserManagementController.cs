﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PagedList;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.BLL.Services.Abstract;
using PrototypeApp.Web.Helpers;
using PrototypeApp.Web.Models.Users;
using PrototypeApp.Web.ViewModels;

namespace PrototypeApp.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserManagementController : Controller
    {
        private const string EmailModelStateKey = "Email";
        private readonly IUserManagementService userManagementService;

        public UserManagementController(IUserManagementService userManagementService)
        {
            this.userManagementService = userManagementService;
        }

        public ActionResult GetUsers(GetUsersModel getUsersModel)
        {
            if (ModelState.IsValid)
            {
                int totalNumberOfUsers;
                var pagination = getUsersModel.PaginationModel;
                if (!PaginationHelper.IsPaginationModelFilled(pagination))
                {
                    PaginationHelper.FillPaginationWithDefaultGridParameters(pagination);
                }

                var filterParameters = Mapper.Map<UserFilterParametersModel, UsersFilterParameters>(getUsersModel.UserFilterParametersModel);
                var requestedUsers = this.userManagementService.Get(filterParameters,
                    pagination.RecordsToSkip,
                    pagination.RecordsPerPage,
                    out totalNumberOfUsers).ToList();

                StaticPagedList<UserDTO> userList = new StaticPagedList<UserDTO>(requestedUsers,
                    pagination.PageNumber,
                    pagination.RecordsPerPage,
                    totalNumberOfUsers);

                return this.View("Users",
                    new GetUsersViewModel()
                    {
                        Users = userList,
                        UserFilterParameters = filterParameters
                    });
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public async Task<ActionResult> AddUser(AddUserModel addUserModal)
        {
            if (ModelState.IsValid)
            {
                var user = Mapper.Map<AddUserModel, UserDTO>(addUserModal);
                var result = await this.userManagementService.AddUserAsync(user, User.Identity.Name);
                if (result.Succeeded)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }

                IdentityHelper.SetModelStateErrors(this.ModelState, result, EmailModelStateKey);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return this.Json(ModelState.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.ToList()));
        }

        [HttpPut]
        public async Task<ActionResult> EditUser(EditedUserModel editedUserModel)
        {
            if (ModelState.IsValid)
            {
                var mappedUserDTO = Mapper.Map<EditedUserModel, UserDTO>(editedUserModel);
                var userEditingResult = await this.userManagementService.EditUserAsync(mappedUserDTO, User.Identity.Name);
                if (userEditingResult.Succeeded)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }

                IdentityHelper.SetModelStateErrors(this.ModelState, userEditingResult, EmailModelStateKey);
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return this.Json(ModelState.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.ToList()));
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteUser(string userId)
        {
            var result = await this.userManagementService.DeleteAsync(userId, User.Identity.Name);
            if (result.Succeeded)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}