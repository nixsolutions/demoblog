﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.Web.Models.BlogPosts;

namespace PrototypeApp.Web.MapperProfiles
{
    public class BlogPostDTOProfile : Profile
    {
        public BlogPostDTOProfile()
        {
            this.CreateMap<BlogPostModel, BlogPostDTO>();
            this.CreateMap<BlogPostDTO, BlogPostModel>();
        }
    }
}