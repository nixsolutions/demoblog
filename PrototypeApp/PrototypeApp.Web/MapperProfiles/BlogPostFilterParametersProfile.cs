﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.Web.Models.BlogPosts;

namespace PrototypeApp.Web.MapperProfiles
{
    public class BlogPostFilterParametersProfile : Profile
    {
        public BlogPostFilterParametersProfile()
        {
            this.CreateMap<BlogPostFilterParametersModel, BlogPostFilterParameters>();
        }
    }
}