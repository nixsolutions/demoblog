﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.Web.Models;
using PrototypeApp.Web.Models.Users;

namespace PrototypeApp.Web.MapperProfiles
{
    public class UserDTOProfile : Profile
    {
        public UserDTOProfile()
        {
            this.CreateMap<LoginModel, UserDTO>();
            this.CreateMap<RegistrationModel, UserDTO>();
            this.CreateMap<EditedUserModel, UserDTO>();
            this.CreateMap<AddUserModel, UserDTO>();
        }
    }
}