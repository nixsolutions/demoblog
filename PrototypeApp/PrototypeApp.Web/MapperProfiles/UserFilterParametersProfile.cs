﻿using AutoMapper;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.Web.Models.Users;

namespace PrototypeApp.Web.MapperProfiles
{
    public class UserFilterParametersProfile : Profile
    {
        public UserFilterParametersProfile()
        {
            this.CreateMap<UserFilterParametersModel, UsersFilterParameters>();
        }
    }
}