﻿using PrototypeApp.Web.Constants;
using PrototypeApp.Web.Models;

namespace PrototypeApp.Web.Helpers
{
    public static class PaginationHelper
    {
        public static bool IsPaginationModelFilled(PaginationModel model)
        {
            return model.RecordsPerPage != default(int);
        }

        public static void FillPaginationWithDefaultGridParameters(PaginationModel model)
        {
            model.RecordsPerPage = PaginationConstants.DefaultRecordsPerGridPage;
        }

        public static void FillPaginationWithDefaultPostParameters(PaginationModel model)
        {
            model.RecordsPerPage = PaginationConstants.DefaultNumberOfPostsOnMainPage;
        }
    }
}