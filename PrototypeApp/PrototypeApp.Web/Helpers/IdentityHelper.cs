﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PrototypeApp.Web.Helpers
{
    public static class IdentityHelper
    {
        public static void SetModelStateErrors(ModelStateDictionary modelState, IdentityResult result, string modelStateKey)
        {
            if (result == null)
            {
                throw new ArgumentNullException();
            }

            if (!result.Succeeded)
            {
                foreach (string error in result.Errors)
                {
                    modelState.AddModelError(modelStateKey, error);
                }
            }
        }
    }
}