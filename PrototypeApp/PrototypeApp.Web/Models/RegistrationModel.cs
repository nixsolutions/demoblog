﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.Web.Validators;

namespace PrototypeApp.Web.Models
{
    [Validator(typeof(RegistrationModelValidator))]
    public class RegistrationModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}