﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrototypeApp.Web.Models.BlogPosts
{
    public class BlogPostFilterParametersModel
    {
        public string Title { get; set; }
    }
}