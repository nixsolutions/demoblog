﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.Web.Validators.BlogPostsValidators;

namespace PrototypeApp.Web.Models.BlogPosts
{
    [Validator(typeof(BlogPostModelValidator))]
    public class BlogPostModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Alias { get; set; }

        public UserDTO Author { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }
    }
}