﻿namespace PrototypeApp.Web.Models.BlogPosts
{
    public class GetPostsModel
    {
        public PaginationModel PaginationModel { get; set; } = new PaginationModel();

        public BlogPostFilterParametersModel FilterParameters { get; set; } = new BlogPostFilterParametersModel();
    }
}