﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.Web.Validators.UsersValidators;

namespace PrototypeApp.Web.Models.Users
{
    [Validator(typeof(EditedUserModelValidator))]
    public class EditedUserModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }
    }
}