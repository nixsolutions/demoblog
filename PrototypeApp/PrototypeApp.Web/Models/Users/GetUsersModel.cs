﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.Web.Validators.UsersValidators;

namespace PrototypeApp.Web.Models.Users
{
    [Validator(typeof(GetUsersModelValidator))]
    public class GetUsersModel
    {
        public PaginationModel PaginationModel { get; set; } = new PaginationModel();

        public UserFilterParametersModel UserFilterParametersModel { get; set; } = new UserFilterParametersModel();
    }
}