﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.Web.Validators;
using PrototypeApp.Web.Validators.UsersValidators;

namespace PrototypeApp.Web.Models.Users
{
    [Validator(typeof(AddUserModelValidator))]
    public class AddUserModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }
    }
}