﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using PrototypeApp.Web.Constants;
using PrototypeApp.Web.Validators;

namespace PrototypeApp.Web.Models
{
    [Validator(typeof(PaginationModelValidator))]
    public class PaginationModel
    {
        public int PageNumber { get; set; } = PaginationConstants.DefaultPageNumber;

        public int RecordsPerPage { get; set; }

        public int RecordsToSkip => (this.PageNumber - 1) * this.RecordsPerPage;
    }
}