﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeApp.BLL.DTOs
{
    public class BlogPostDTO
    {
        public int Id { get; set; }

        public UserDTO Author { get; set; }

        public string Title { get; set; }

        public string Alias { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }
    }
}