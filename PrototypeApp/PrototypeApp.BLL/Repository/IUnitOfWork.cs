﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;

namespace PrototypeApp.BLL.Repository
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }

        IRepository<BlogPost> BlogPosts { get; }

        Task SaveAsync();
    }
}