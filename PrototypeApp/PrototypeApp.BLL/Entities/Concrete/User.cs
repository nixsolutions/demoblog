﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PrototypeApp.BLL.Entities.Concrete
{
    public class User : IdentityUser
    {
        public DateTime CreationDate { get; set; }
    }
}