﻿using System;
using PrototypeApp.BLL.Entities.Abstract;

namespace PrototypeApp.BLL.Entities.Concrete
{
    public class BlogPost : BaseEntity
    {
        public virtual User Author { get; set; }

        public string Title { get; set; }

        public string Alias { get; set; }

        public string Content { get; set; }

        public DateTime CreationDate { get; set; }
    }
}