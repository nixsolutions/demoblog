﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;

namespace PrototypeApp.BLL.Services.Abstract
{
    public interface IUserService
    {
        Task<IdentityResult> RegisterAsync(UserDTO user);

        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
    }
}