﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.BLL.Services.Abstract
{
    public interface IBlogPostService
    {
        IEnumerable<BlogPostDTO> Get(BlogPostFilterParameters filterParameters, int postsToSkip, int postsToTake, out int totalNumberOfPosts);

        BlogPostDTO GetBlogPostById(int postId);

        BlogPostDTO GetBlogPostByAlias(string alias);

        Task Add(BlogPostDTO blogPostDTO, string currentUserEmail);

        Task Edit(BlogPostDTO blogPostDTO, string currentUserEmail);

        Task Delete(int postId, string currentUserEmail);

        bool IsAliasTaken(string alias, int currentPostId);

        bool IsTitleTaken(string title, int currentPostId);
    }
}