﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.BLL.Services.Abstract
{
    public interface IUserManagementService
    {
        IEnumerable<UserDTO> Get(UsersFilterParameters filterParameters, int usersToSkip, int usersToTake, out int totalNumberOfUsers);

        Task<IdentityResult> DeleteAsync(string userId, string currentUserEmail);

        Task<IdentityResult> EditUserAsync(UserDTO editedUserDTO, string currentUserEmail);

        Task<IdentityResult> AddUserAsync(UserDTO userDTO, string currentUserEmail);
    }
}