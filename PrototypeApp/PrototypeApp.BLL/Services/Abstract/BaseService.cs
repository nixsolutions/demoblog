﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Resources;

namespace PrototypeApp.BLL.Services.Abstract
{
    public class BaseService
    {
        protected readonly UserManager<User> UserManager;

        public BaseService(UserManager<User> userManager)
        {
            this.UserManager = userManager;
        }

        protected async Task<IdentityResult> AddUser(User user, string password, string role)
        {
            var result = await this.UserManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                return await this.UserManager.AddToRoleAsync(user.Id, role);
            }

            return result;
        }

        protected async Task<bool> IsUserAdmin(string username)
        {
            var user = await this.UserManager.FindByNameAsync(username);
            return await this.UserManager.IsInRoleAsync(user.Id, Roles.Admin);
        }
    }
}