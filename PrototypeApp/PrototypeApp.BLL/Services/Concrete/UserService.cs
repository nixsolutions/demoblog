﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Resources;
using PrototypeApp.BLL.Services.Abstract;

namespace PrototypeApp.BLL.Services.Concrete
{
    public class UserService : BaseService, IUserService
    {
        public UserService(UserManager<User> userManager) : base(userManager)
        {
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDTO)
        {
            User user = await this.UserManager.FindAsync(userDTO.Email, userDTO.Password);
            if (user == null)
            {
                return null;
            }

            return await this.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public async Task<IdentityResult> RegisterAsync(UserDTO userDTO)
        {
            User user = new User { UserName = userDTO.Email, CreationDate = DateTime.Now };
            return await this.AddUser(user, userDTO.Password, Roles.User);
        }
    }
}