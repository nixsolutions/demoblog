﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.Abstract;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.BLL.Repository;
using PrototypeApp.BLL.Resources;
using PrototypeApp.BLL.Services.Abstract;

namespace PrototypeApp.BLL.Services.Concrete
{
    public class UserManagementService : BaseService, IUserManagementService
    {
        private IUnitOfWork unitOfWork;
        private IFilter<User, UsersFilterParameters> userFilter;

        public UserManagementService(IUnitOfWork unitOfWork, IFilter<User, UsersFilterParameters> userFilter, UserManager<User> userManager)
            : base(userManager)
        {
            this.unitOfWork = unitOfWork;
            this.userFilter = userFilter;
        }

        public IEnumerable<UserDTO> Get(UsersFilterParameters filterParameters, int usersToSkip, int usersToTake, out int totalNumberOfUsers)
        {
            var filteredUsers = this.userFilter.Filter(this.unitOfWork.Users.Entities, filterParameters);
            totalNumberOfUsers = filteredUsers.Count();
            var requestedUsers = filteredUsers.OrderBy(user => user.UserName)
                .Skip(usersToSkip).Take(usersToTake).ToList().Select(Mapper.Map<User, UserDTO>).ToList();
            this.FillUsersRoles(requestedUsers);
            return requestedUsers;
        }

        public async Task<IdentityResult> DeleteAsync(string userId, string currentUserEmail)
        {
            var user = await this.UserManager.FindByIdAsync(userId);
            if (user.UserName == currentUserEmail)
            {
                return IdentityResult.Failed();
            }

            return await this.UserManager.DeleteAsync(user);
        }

        public async Task<IdentityResult> EditUserAsync(UserDTO editedUserDTO, string currentUserEmail)
        {
            var userToEdit = await this.UserManager.FindByIdAsync(editedUserDTO.Id);
            if (userToEdit == null)
            {
                return IdentityResult.Failed();
            }

            if (!await this.IsUserAdmin(currentUserEmail))
            {
                throw new InvalidOperationException(ErrorMessages.UserIsNotAllowedToDoThis);
            }

            userToEdit.UserName = editedUserDTO.Email;
            var updateUserResult = await this.UserManager.UpdateAsync(userToEdit);
            if (!updateUserResult.Succeeded)
            {
                return updateUserResult;
            }

            var currentUserRole = this.UserManager.GetRoles(userToEdit.Id).FirstOrDefault();
            if (currentUserRole != editedUserDTO.Role)
            {
                this.UserManager.RemoveFromRole(userToEdit.Id, currentUserRole);
                this.UserManager.AddToRole(userToEdit.Id, editedUserDTO.Role);
            }

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> AddUserAsync(UserDTO userDTO, string currentUserEmail)
        {
            if (!await this.IsUserAdmin(currentUserEmail))
            {
                throw new InvalidOperationException(ErrorMessages.UserIsNotAllowedToDoThis);
            }

            User newUser = new User { UserName = userDTO.Email, CreationDate = DateTime.Now };
            return await this.AddUser(newUser, userDTO.Password, userDTO.Role);
        }

        private void FillUsersRoles(IList<UserDTO> users)
        {
            foreach (var user in users)
            {
                user.Role = this.UserManager.GetRoles(user.Id).FirstOrDefault();
            }
        }
    }
}