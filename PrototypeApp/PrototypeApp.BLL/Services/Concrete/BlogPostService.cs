﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Filters;
using PrototypeApp.BLL.Filters.Abstract;
using PrototypeApp.BLL.Filters.FilterParameters;
using PrototypeApp.BLL.Repository;
using PrototypeApp.BLL.Resources;
using PrototypeApp.BLL.Services.Abstract;

namespace PrototypeApp.BLL.Services.Concrete
{
    public class BlogPostService : BaseService, IBlogPostService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IFilter<BlogPost, BlogPostFilterParameters> blogPostFilter;

        public BlogPostService(IUnitOfWork unitOfWork,
            IFilter<BlogPost, BlogPostFilterParameters> blogPostFilter,
            UserManager<User> userManager) : base(userManager)
        {
            this.unitOfWork = unitOfWork;
            this.blogPostFilter = blogPostFilter;
        }

        public IEnumerable<BlogPostDTO> Get(BlogPostFilterParameters filterParameters,
            int postsToSkip,
            int postsToTake,
            out int totalNumberOfPosts)
        {
            var filteredBlogPosts = this.blogPostFilter.Filter(this.unitOfWork.BlogPosts.Entities, filterParameters);
            totalNumberOfPosts = filteredBlogPosts.Count();
            var requestedPosts = filteredBlogPosts.OrderByDescending(blogPost => blogPost.CreationDate)
                .Skip(postsToSkip).Take(postsToTake).ToList().Select(Mapper.Map<BlogPost, BlogPostDTO>);
            return requestedPosts;
        }

        public async Task Add(BlogPostDTO blogPostDTO, string currentUserEmail)
        {
            if (!await this.IsUserAdmin(currentUserEmail))
            {
                throw new InvalidOperationException(ErrorMessages.UserIsNotAllowedToDoThis);
            }

            if (blogPostDTO == null)
            {
                throw new ArgumentNullException(ErrorMessages.BlogPostIsNull);
            }

            var blogPost = Mapper.Map<BlogPostDTO, BlogPost>(blogPostDTO);
            blogPost.CreationDate = DateTime.Now;
            blogPost.Author = await this.UserManager.FindByNameAsync(currentUserEmail);
            this.unitOfWork.BlogPosts.Insert(blogPost);
            await this.unitOfWork.SaveAsync();
        }

        public async Task Edit(BlogPostDTO blogPostDTO, string currentUserEmail)
        {
            if (!await this.IsUserAdmin(currentUserEmail))
            {
                throw new InvalidOperationException(ErrorMessages.UserIsNotAllowedToDoThis);
            }

            if (blogPostDTO == null)
            {
                throw new ArgumentNullException(ErrorMessages.BlogPostIsNull);
            }

            var blogPost = this.unitOfWork.BlogPosts.GetById(blogPostDTO.Id);
            if (blogPost == null)
            {
                throw new ArgumentException(ErrorMessages.NoPostWithSuchId);
            }

            var blogPostAutor = blogPost.Author;
            Mapper.Map(blogPostDTO, blogPost);
            blogPost.Author = blogPostAutor;
            await this.unitOfWork.SaveAsync();
        }

        public async Task Delete(int postId, string currentUserEmail)
        {
            var postToDelete = this.unitOfWork.BlogPosts.GetById(postId);
            if (postToDelete == null)
            {
                throw new ArgumentException(ErrorMessages.NoPostWithSuchId);
            }

            if (!await this.IsUserAdmin(currentUserEmail))
            {
                throw new InvalidOperationException(ErrorMessages.UserIsNotAllowedToDoThis);
            }

            this.unitOfWork.BlogPosts.Delete(postToDelete);
            await this.unitOfWork.SaveAsync();
        }

        public BlogPostDTO GetBlogPostById(int postId)
        {
            var requestedBlogPost = this.unitOfWork.BlogPosts.GetById(postId);
            return Mapper.Map<BlogPost, BlogPostDTO>(requestedBlogPost);
        }

        public BlogPostDTO GetBlogPostByAlias(string alias)
        {
            var requestedBlogPost = this.unitOfWork.BlogPosts.Find(post => post.Alias == alias).FirstOrDefault();
            return Mapper.Map<BlogPost, BlogPostDTO>(requestedBlogPost);
        }

        public bool IsAliasTaken(string alias, int currentPostId)
        {
            if (currentPostId != default(int))
            {
                return this.unitOfWork.BlogPosts.Find(post => post.Alias == alias && post.Id != currentPostId).Any();
            }

            return this.unitOfWork.BlogPosts.Find(post => post.Alias == alias).Any();
        }

        public bool IsTitleTaken(string title, int currentPostId)
        {
            if (currentPostId != default(int))
            {
                return this.unitOfWork.BlogPosts.Find(post => post.Title == title && post.Id != currentPostId).Any();
            }

            return this.unitOfWork.BlogPosts.Find(post => post.Title == title).Any();
        }
    }
}