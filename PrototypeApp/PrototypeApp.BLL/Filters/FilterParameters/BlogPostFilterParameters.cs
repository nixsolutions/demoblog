﻿namespace PrototypeApp.BLL.Filters.FilterParameters
{
    public class BlogPostFilterParameters
    {
        public string Title { get; set; }
    }
}