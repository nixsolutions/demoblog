﻿using System;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Filters.Abstract;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.BLL.Filters.Concrete
{
    public class UserFilter : IFilter<User, UsersFilterParameters>
    {
        public IQueryable<User> Filter(IQueryable<User> usersCollection, UsersFilterParameters filterParameters)
        {
            if (filterParameters == null)
            {
                return usersCollection;
            }

            var usersFilteringPredicate = PredicateBuilder.New<User>(user => true);
            this.AddEmailFiltering(usersFilteringPredicate, filterParameters.Email);
            return usersCollection.AsExpandable().Where(usersFilteringPredicate);
        }

        private void AddEmailFiltering(ExpressionStarter<User> predicate, string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                predicate = predicate.And(user => user.UserName.Contains(email));
            }
        }
    }
}