﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqKit;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Filters.Abstract;
using PrototypeApp.BLL.Filters.FilterParameters;

namespace PrototypeApp.BLL.Filters.Concrete
{
    public class BlogPostFilter : IFilter<BlogPost, BlogPostFilterParameters>
    {
        public IQueryable<BlogPost> Filter(IQueryable<BlogPost> blogPostsCollection, BlogPostFilterParameters filterParameters)
        {
            if (filterParameters == null)
            {
                return blogPostsCollection;
            }

            var blogPostsFilteringPredicate = PredicateBuilder.New<BlogPost>(blogPost => true);
            this.AddTitleFiltering(blogPostsFilteringPredicate, filterParameters.Title);
            return blogPostsCollection.AsExpandable().Where(blogPostsFilteringPredicate);
        }

        private void AddTitleFiltering(ExpressionStarter<BlogPost> predicate, string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                predicate = predicate.And(blogPost => blogPost.Title.Contains(title));
            }
        }
    }
}