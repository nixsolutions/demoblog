﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeApp.BLL.Filters.Abstract
{
    public interface IFilter<TModel, TFilterParameters>
    {
        IQueryable<TModel> Filter(IQueryable<TModel> collection, TFilterParameters filterParameters);
    }
}