﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;

namespace PrototypeApp.BLL.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            this.CreateMap<User, UserDTO>().ForMember(nameof(UserDTO.Email), opt => opt.MapFrom(user => user.UserName));
            this.CreateMap<UserDTO, User>();
        }
    }
}