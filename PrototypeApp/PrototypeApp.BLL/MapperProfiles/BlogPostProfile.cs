﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PrototypeApp.BLL.DTOs;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;

namespace PrototypeApp.BLL.MapperProfiles
{
    public class BlogPostProfile : Profile
    {
        public BlogPostProfile()
        {
            this.CreateMap<BlogPost, BlogPostDTO>();
            this.CreateMap<BlogPostDTO, BlogPost>();
        }
    }
}