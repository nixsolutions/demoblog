﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Repository;

namespace PrototypeApp.DAL.Repository
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly IdentityDbContext<User> context;
        private IRepository<User> users;
        private IRepository<BlogPost> blogPosts;

        public EFUnitOfWork(IdentityDbContext<User> context)
        {
            this.context = context;
        }

        public async Task SaveAsync() => await this.context.SaveChangesAsync();

        public IRepository<User> Users => this.users ?? (this.users = new EFRepository<User>(this.context));

        public IRepository<BlogPost> BlogPosts => this.blogPosts ?? (this.blogPosts = new EFRepository<BlogPost>(this.context));
    }
}