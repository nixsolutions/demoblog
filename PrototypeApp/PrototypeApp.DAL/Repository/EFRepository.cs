﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Repository;

namespace PrototypeApp.DAL.Repository
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        private readonly IdentityDbContext<User> context;

        public EFRepository(IdentityDbContext<User> context)
        {
            this.context = context;
        }

        public IQueryable<T> Entities => this.context.Set<T>();

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate) => this.context.Set<T>().Where(predicate);

        public T GetById(int id) => this.context.Set<T>().Find(id);

        public void Insert(T entity) => this.context.Set<T>().Add(entity);

        public void Delete(T entity) => this.context.Set<T>().Remove(entity);
    }
}