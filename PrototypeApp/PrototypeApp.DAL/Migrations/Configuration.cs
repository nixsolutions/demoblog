namespace PrototypeApp.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using PrototypeApp.DAL.Context;

    internal sealed class Configuration : DbMigrationsConfiguration<PrototypeApp.DAL.Context.ApplicationContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
        }
    }
}