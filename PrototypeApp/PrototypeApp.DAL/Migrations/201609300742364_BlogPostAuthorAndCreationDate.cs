namespace PrototypeApp.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class BlogPostAuthorAndCreationDate : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.BlogPosts", "CreationDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.BlogPosts", "Author_Id", c => c.String(maxLength: 128));
            this.CreateIndex("dbo.BlogPosts", "Author_Id");
            this.AddForeignKey("dbo.BlogPosts", "Author_Id", "dbo.AspNetUsers", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.BlogPosts", "Author_Id", "dbo.AspNetUsers");
            this.DropIndex("dbo.BlogPosts", new[] { "Author_Id" });
            this.DropColumn("dbo.BlogPosts", "Author_Id");
            this.DropColumn("dbo.BlogPosts", "CreationDate");
        }
    }
}