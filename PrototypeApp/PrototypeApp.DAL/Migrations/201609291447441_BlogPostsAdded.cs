namespace PrototypeApp.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class BlogPostsAdded : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.BlogPosts",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(),
                    Aliast = c.String(),
                    Content = c.String(),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            this.DropTable("dbo.BlogPosts");
        }
    }
}