namespace PrototypeApp.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class NamingFixes : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.BlogPosts", "Alias", c => c.String());
            this.DropColumn("dbo.BlogPosts", "Aliast");
        }

        public override void Down()
        {
            this.AddColumn("dbo.BlogPosts", "Aliast", c => c.String());
            this.DropColumn("dbo.BlogPosts", "Alias");
        }
    }
}