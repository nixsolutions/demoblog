﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using DatabaseType = System.Data.Entity.Database;

namespace PrototypeApp.DAL.Context
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<BlogPost> BlogPosts { get; set; }

        static ApplicationContext()
        {
            DatabaseType.SetInitializer(new DatabaseInitializer());
        }

        public ApplicationContext() : base("PrototypeAppConnectionString")
        {
        }
    }
}