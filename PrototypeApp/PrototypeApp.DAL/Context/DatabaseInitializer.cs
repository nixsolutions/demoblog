﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PrototypeApp.BLL.Entities;
using PrototypeApp.BLL.Entities.Concrete;
using PrototypeApp.BLL.Resources;

namespace PrototypeApp.DAL.Context
{
    internal class DatabaseInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            base.Seed(context);
            this.InitializeRoles(context);
            this.AddAdministratorToDatabase(context);
        }

        private void InitializeRoles(ApplicationContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            roleManager.Create(new IdentityRole { Name = Roles.Admin });
            roleManager.Create(new IdentityRole { Name = Roles.User });
        }

        private void AddAdministratorToDatabase(ApplicationContext context)
        {
            var userManager = new UserManager<User>(new UserStore<User>(context));
            var admin = new User() { UserName = "admin@m.com" };
            admin.CreationDate = DateTime.Now;
            var result = userManager.Create(admin, "qwerty");
            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, Roles.Admin);
            }

            context.SaveChanges();
        }
    }
}